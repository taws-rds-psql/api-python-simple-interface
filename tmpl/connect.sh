#!/bin/bash
## create gitlab connection 
kubectl config use-context taws-rds-psql/api-python-simple-interface:api-python-deploy --namespace=aws-ecr

kubectl config set-context --current --namespace=aws-ecr

echo "launch all yaml files  from tmpl ..." #   генерируется запуск файлов YAML


kubectl apply -f tmpl/deploy.yaml
kubectl apply -f tmpl/service.yaml

kubectl rollout status -n "aws-ecr" -w "deploy/api-py"
