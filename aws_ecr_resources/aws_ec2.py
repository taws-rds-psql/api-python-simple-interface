#!/usr/bin/env python3

## Lets create EC2 instance using Python BOTO3

## first: need to create virtual environmen ENV
# python3 -m venv fl_venv 
## Next: Have to activate VENV 
# . fl_venv/bin/activatepython3 -m venv fl_venv
# install BOTO3

## To create an EC2 instance using Python3 and Boto3, you can use the following code:

import boto3

# Set up client
ec2 = boto3.client('ec2')

# Launch instance
instance = ec2.run_instances(
    ImageId='ami-0ec7f9846da6b0f61',
    InstanceType='t2.nano',
    MinCount=1,
    MaxCount=1
)

print(f"Instance ID: {instance['Instances'][0]['InstanceId']}")

# import boto3

# def create_ec2_instance():
#   try:
#     print ("Createing EC2 Instance")
#     resource_ec2 = boto3.client("ec2")
#     resource_ec2.run_instances(
#         ImageId="ami-0ec7f9846da6b0f61",
#         MinCount=1,
#         MaxCount=1,
#         instanceType="t2.nano",
#         KeyName="key",
#         SubnetIds="subnet-b4f153f8",
#         VpcId="vpc-5fb10835",
#         SecurityGroupIds="sg-8558c9fa"
#     )
#   except Exception as e:
#         print(e)
# create_ec2_instance()